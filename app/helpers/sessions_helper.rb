module SessionsHelper
  def current_user
    return nil unless session[:username]
    @current_user ||= User.find_by_username(session[:username])
  end

  def signin(user)
    session[:username] = user.username
  end

  def signout
    session[:username] = nil
  end

  def logged_in?
    !!current_user
  end

  def require_current_user
    redirect_to login_url unless logged_in?
  end
end
