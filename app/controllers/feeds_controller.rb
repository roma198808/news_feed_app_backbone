class FeedsController < ApplicationController
  before_filter :require_current_user

  def index
    @feeds = current_user.feeds

    @feeds.each do |feed|
      feed.reload
    end

    respond_to do |format|
      format.html { render :index }
      format.json { render :json => @feeds.as_json(include: :entries) }
    end
  end

  def create
    feed = Feed.find_or_create_by_url(params[:feed][:url])
    if feed
      render :json => feed
    else
      render :json => { error: "invalid url" }, status: :unprocessable_entity
    end
  end

  def update
    feed = Feed.find(params[:id])
    feed.update_attributes(params[:feed])
    render json: feed
  end
end
