class EntriesController < ApplicationController
  def index
    feed = Feed.find(params[:feed_id])
    feed.reload
    render :json => feed.entries.order("created_at DESC").limit(5)
  end
end
