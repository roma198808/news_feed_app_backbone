class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by_username(params[:user][:username])
    signin(@user)
    redirect_to feeds_url
  end

  def destroy
    signout
    redirect_to new_session_url
  end

end
