NewReader.Collections.Entries = Backbone.Collection.extend({
  initialize: function (entries, feedId) {
    this.url = "/feeds/" + feedId + "/entries";
  },

  model: NewReader.Models.Entry
});