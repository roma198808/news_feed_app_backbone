NewReader.Collections.Feeds = Backbone.Collection.extend({
  model: NewReader.Models.Feed,
  url: '/feeds',

  parse: function (response) {
    var that = this;
    _.each(response, function(feed) {
      var feedModel = new NewReader.Models.Feed(feed)
      that.add(feedModel);
      feedModel.entries = new NewReader.Collections.Entries(feed.entries, feed.id);
    });
  }
});
