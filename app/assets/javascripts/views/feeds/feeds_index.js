NewReader.Views.FeedsIndex = Backbone.View.extend({
  template: JST['feeds/index'],

  events: {
    "click #favorite": "toggleFavorite",
    "click #unfavorite": "toggleFavorite"
  },

  render: function() {
    var rendered = this.template({ feeds: this.collection });
    this.$el.html(rendered);
    return this;
  },

  toggleFavorite: function(event) {
    event.preventDefault();
    var feedId = $(event.target).closest('li.feed').attr('data-id');
    var feed = this.collection.get(feedId);
    var opposite = !feed.get('favorite')

    feed.save({favorite: opposite},{
      success: function () {
        $(event.target).closest('li.feed').removeClass().addClass("feed " + opposite)
      }
    });
  }
});
