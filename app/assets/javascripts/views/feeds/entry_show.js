NewReader.Views.EntryShow = Backbone.View.extend({
  template: JST['feeds/entry_show'],

  render: function() {
    var rendered = this.template({ entry: this.model });
    this.$el.html(rendered);
    return this;
  }
});
