NewReader.Views.FeedsShow = Backbone.View.extend({
  initialize: function() {
    this.listenTo(this.collection, "all", this.render)
  },

  template: JST['feeds/show'],

  events: {
    "click #refresh": "refreshFeed"
  },

  render: function() {
    var rendered = this.template({
      feed: this.model,
      entries: this.collection
    });
    this.$el.html(rendered);
    return this;
  },

  refreshFeed: function(event) {
    event.preventDefault();
    this.collection.fetch();
  }
});