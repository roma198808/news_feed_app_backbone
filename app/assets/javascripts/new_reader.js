window.NewReader = {
  Models: {},
  Collections: {},
  Views: {},
  Routers: {},

  initialize: function(feeds) {
    var feeds = new NewReader.Collections.Feeds(feeds, {parse: true});
    new NewReader.Routers.Feeds(feeds);
    Backbone.history.start();
  }
};