NewReader.Routers.Feeds = Backbone.Router.extend({
  initialize: function (feeds) {
    this.collection = feeds;
  },

  routes: {
    '': 'index',
    'feeds/:id': 'show',
    'feeds/:feed_id/entries/:id': 'showEntry'
  },

  index: function() {
    var view = new NewReader.Views.FeedsIndex({
      collection: this.collection
    });
     $("#content").html(view.render().$el);
  },

  show: function(id) {
    var feed = this.collection.get(id);
    var entries = feed.entries;

    var view = new NewReader.Views.FeedsShow({
      model: feed,
      collection: entries
    });

    this._swapViewShow(view);
  },

  showEntry: function(feedId, id) {
    var feed = this.collection.get(feedId);
    var view = new NewReader.Views.EntryShow({
      model: feed.entries.get(id)
    });
    this._swapViewEntry(view);
  },

  _swapViewShow: function (view) {
    this._currentFeed && this._currentFeed.remove();
    this._currentFeed = view;
    $("#show").html(view.render().$el);
    this.index();
  },

  _swapViewEntry: function (view) {
    this._currentEntry && this._currentEntry.remove();
    this._currentEntry = view;
    $("#entry").html(view.render().$el);
    this.index();
  }

});
