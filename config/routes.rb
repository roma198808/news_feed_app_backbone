NewReader::Application.routes.draw do
  resources :feeds, only: [:index, :create, :update] do
    resources :entries, only: [:index]
  end

  resource :session, only: [:new, :create, :destroy]

  root to: "feeds#index"
end
